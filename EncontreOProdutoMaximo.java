import java.util.ArrayList;

public class EncontreOProdutoMaximo {
	public static void main(String[] args) {
		int[] numeros = {0,1,-1,-2}; //insira a entrada aqui
		int[] negativos = new int[numeros.length];
		int count=0;
		int countZeros=0;
		/*
		* "produto" possui tipo double para o caso de
		* evitar overflow na multiplicacao de dois numeros
		* inteiros de grande magnitude.
		*/
		
		//Caso de nao existir elementos no array ou existir apenas 1 elemento
		if(numeros.length == 0 || numeros.length == 1)
		{
			System.out.println("Quantidade de elementos insuficiente"
					+ " para realizar uma combinacao.");
		}else
		{
			for(int i=0; i<numeros.length; i++)
			{
				//numeros positivos podem ser combinados entre si
				if(numeros[i] > 0)
				{
					System.out.print(numeros[i] + " ");
				}else if(numeros[i]<0)
				{
					negativos[count] = numeros[i];
					count++;
				}else if(numeros[i] == 0)
				{
					countZeros++;
				}
				
				/*
				 * Se o array inteiro eh formado por zeros,
				 * entao o maior produto eh zero.
				 */
				if(countZeros == numeros.length)
					System.out.print(0);
			}
			
			
			/*
			 * Se houver uma quantidade par de numeros negativos,
			 * estes podem ser considerados no produto.
			 */
			if(count % 2 == 0)
			{
				for(int i=0; i<count; i++)
				{
					System.out.print(negativos[i] + " ");
				}
			/*
			 * Se houver uma quantidade impar e maior que 1 de
			 * numeros negativos, estes podem ser combinados no produto,
			 * descartando-se o maior deles se se deseja obter o produto
			 * maximo.	
			 */
			}else if(count > 2)
			{
				int maior=negativos[0];
				int indiceMenor=0;
				
				//Encontrando o maior numero negativo.
				for(int i=1; i<count;i++)
				{
					if(negativos[i] > maior)
					{
						maior=negativos[i];
						indiceMenor = i;
					}
				}
				//Combinando os numeros negativos restantes com os positivos
				int flag = 0; 
				for(int i=0; i<count; i++)
				{
					if(flag == 0)
					{
						if(negativos[i] != negativos[indiceMenor])
							System.out.print(negativos[i] + " ");
						else
							flag = 1;
					/*
					 * Caso em que tenho uma qtd par de numeros negativos
					 * ,2 deles sao iguais e menores que os demais.
					 */
					}else
					{
						System.out.print(negativos[i] + " ");
					}

				}
			}
		}
		
	}
}
